#!/usr/bin/python
import smtplib
import configparser

class ReportToEmail:

    filediconfigurazione = '/home/dall_test/ConfigFile.properties'
    # filediconfigurazione = './ConfigFile.properties'

    config = configparser.RawConfigParser()
    config.read(filediconfigurazione)

    def sendemail(self, subject, message):
        header = 'From: %s\n' % 'progettird.alkemy@gmail.com'
        # header += 'To: %s\n' % ','.join(['dall@mailinator.com'])
        # header += 'Cc: %s\n' % ','.join(['antonio.mongiardo@alkemy.com','pasquale.piane@alkemy.com'])
        header += 'Subject: %s\n\n' % subject
        message = header + message

        server = smtplib.SMTP('smtp.gmail.com:587')
        server.starttls()
        server.login('progettird.alkemy@gmail.com', 'ProgettiRD01')
        dest1 = self.config.get('DESTINATARI_EMAIL_REPORT', 'destinatarioReport1')
        dest2 = self.config.get('DESTINATARI_EMAIL_REPORT', 'destinatarioReport2')
        dest3 = self.config.get('DESTINATARI_EMAIL_REPORT', 'destinatarioReport3')
        dest4 = self.config.get('DESTINATARI_EMAIL_REPORT', 'destinatarioReport4')
        dest5 = self.config.get('DESTINATARI_EMAIL_REPORT', 'destinatarioReport5')
        dest6 = self.config.get('DESTINATARI_EMAIL_REPORT', 'destinatarioReport6')
        dest7 = self.config.get('DESTINATARI_EMAIL_REPORT', 'destinatarioReport7')
        dest8 = self.config.get('DESTINATARI_EMAIL_REPORT', 'destinatarioReport8')
        dest9 = self.config.get('DESTINATARI_EMAIL_REPORT', 'destinatarioReport9')
        dest10 = self.config.get('DESTINATARI_EMAIL_REPORT', 'destinatarioReport10')
        destinatari = [dest1, dest2, dest3, dest4, dest5, dest6, dest7, dest8, dest9, dest10]
        problems = server.sendmail('progettird.alkemy@gmail.com', destinatari, message)
        server.quit()
        return problems


ReportToEmail().sendemail('test','test ok')