#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
import time, random
import re
import configparser
nomeTest = 'id-04_LoginEditoreConControlloAfresco.py'
print('START '+nomeTest)


# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)


# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")


driver.implicitly_wait(100)

def test_login_editore_con_controllo_afresco():
    time.sleep(3)
    print("sto per aprire d-all")
    driver.get("http://dall.rd.alkemylab.it/")
    print("ho aperto d-all")
    time.sleep(5)
    print("sto per cliccare su menu")
    try:
        element = WebDriverWait(driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="open-menu-button"]/button'))
        )
        driver.find_element_by_xpath('//*[@id="open-menu-button"]/button').click()
        print("ho cliccato su menu")
    finally:
        time.sleep(3)

    print("clicco su 'Accedi'")
    driver.find_element_by_css_selector("#menu-item-82 > a").click()
    time.sleep(3)

    print("Sto per inserire l'username")
    driver.find_element_by_id('user_login').click()
    driver.find_element_by_id('user_login').send_keys("Editore")
    time.sleep(2)

    print("Sto per inserire la password")
    driver.find_element_by_id('user_pass').click()
    driver.find_element_by_id('user_pass').send_keys("Editore00e")
    time.sleep(2)

    print("effettuo il login")
    driver.find_element_by_id('wp-submit').click()
    time.sleep(3)

    try:
        # assert di login eseguito controllando che ci sia COSA VUOI FARE?
        assert "COSA VUOI FARE?" in driver.find_element_by_css_selector('#selenium-highlight > div > h1').text
        print("LOGIN OK")
    finally:
        time.sleep(3)

    print("Clicco su 'Repository Documentale'")
    driver.find_element_by_css_selector('#selenium-highlight > div > div.tabella_cosa_vuoi_fare > table > tbody > tr:nth-child(1) > td:nth-child(1)').click()
    time.sleep(5)
    print("Sono su 'Afresco'")

    try:
        time.sleep(2)
        element = WebDriverWait(driver,50).until(
            EC.presence_of_element_located((By.LINK_TEXT, 'D-All'))
        )
        driver.find_element_by_link_text("D-All").click()
        print("ho cliccato sul link del sito")
    finally:
        time.sleep(3)
        print("sto per cliccare su 'Raccolta Documenti'")

    try:
        time.sleep(2)
        element = WebDriverWait(driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '//div[2]/div/div/div[2]/span/a'))
        )
        driver.find_element_by_xpath("//div[2]/div/div/div[2]/span/a").click()
        print("ho cliccato su 'Raccolta Documenti'")
    finally:
        time.sleep(3)
        print("verifico di essere approdato sulla pagina corretta")

    try:
        assert "OR 1" in driver.find_element_by_link_text("OR 1").text
        print("Caricamento pagina corretto")
    finally:
        time.sleep(3)

    print("sto per cliccare su 'Crea'")
    driver.find_element_by_xpath("//div[2]/span/span/button").click()
    print("ho cliccato su 'Crea'")
    time.sleep(3)
    driver.find_element_by_css_selector("span.folder-file").click()
    time.sleep(3)

    casuale_1 = [random.choice('abcdefghilmnopqrstuvz') for _ in range(3)]
    casuale = ''.join(casuale_1)
    cas = casuale.upper()

    try:
        time.sleep(2)
        element = WebDriverWait(driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="template_x002e_documentlist_v2_x002e_documentlibrary_x0023_default-createFolder_prop_cm_name"]'))
        )
        driver.find_element_by_xpath('//*[@id="template_x002e_documentlist_v2_x002e_documentlibrary_x0023_default-createFolder_prop_cm_name"]').click()
        print("ho cliccato su 'Nome'")
        driver.find_element_by_xpath(
            '//*[@id="template_x002e_documentlist_v2_x002e_documentlibrary_x0023_default-createFolder_prop_cm_name"]').send_keys("LabRendeAutore"+cas)
        print("ho inserito il nome della cartella")
    finally:
        time.sleep(3)
        print("sto per salvare la crtella creata")

    driver.find_element_by_css_selector('#template_x002e_documentlist_v2_x002e_documentlibrary_x0023_default-createFolder-form-submit-button').click()
    time.sleep(2)
    print("Seleziono la cartella creata per eliminarla")
    driver.find_element_by_xpath("//td/div/input").click()
    time.sleep(3)
    driver.find_element_by_xpath('//*[@id="template_x002e_documentlist_v2_x002e_documentlibrary_x0023_default-selectedItems-button"]/span').click()
    time.sleep(5)
    driver.find_element_by_css_selector("span.onActionDelete").click()
    time.sleep(3)
    print("Sto per confermare l'eliminazione della cartella creata")
    driver.find_element_by_xpath("//button").click()
    print("Ho eliminato la cartella creata")




test_login_editore_con_controllo_afresco()

# driver.quit()
print("END " + nomeTest)


#invio mail di report
from ReportToEmail import ReportToEmail
report = ReportToEmail()

data = time.strftime("%d/%m/%Y - ore %H:%M:%S")
oggetto = nomeTest+' - OK - eseguito il '+data
messaggio = 'END '+nomeTest
report.sendemail(subject = oggetto, message = messaggio)

unittest.main()