#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import per bamboo
import configparser
import unittest

from pyvirtualdisplay import Display

from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
import time
import re
import configparser

nomeTest = 'id-05_NavigateSiteEditore.py'
print('START ' + nomeTest)

# SET for Bamboo
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--disable-web-security")
options.add_argument('--no-sandbox')
options.add_argument("--disable-setuid-sandbox")
options.add_argument("--window-size=1920,1080")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(chrome_options=options)

# SET for TestDeveloper
# driver = webdriver.Chrome()
# driver.maximize_window()

driver.delete_all_cookies()
print ("eliminati tutti i cookies")

driver.implicitly_wait(100)


def test_navigate_site_editore():
    time.sleep(3)
    print("sto per aprire d-all")
    driver.get("http://dall.rd.alkemylab.it/")
    print("ho aperto d-all")
    time.sleep(5)
    print("sto per cliccare su menu")
    try:
        element = WebDriverWait(driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="open-menu-button"]/button'))
        )
        driver.find_element_by_xpath('//*[@id="open-menu-button"]/button').click()
        print("ho cliccato su menu")
    finally:
        time.sleep(3)

    print("clicco su 'Accedi'")
    driver.find_element_by_css_selector("#menu-item-82 > a").click()
    time.sleep(3)

    print("Sto per inserire l'username")
    driver.find_element_by_id('user_login').click()
    driver.find_element_by_id('user_login').send_keys("Editore")
    time.sleep(2)

    print("Sto per inserire la password")
    driver.find_element_by_id('user_pass').click()
    driver.find_element_by_id('user_pass').send_keys("Editore00e")
    time.sleep(2)

    print("effettuo il login")
    driver.find_element_by_id('wp-submit').click()
    time.sleep(3)

    try:
        # assert di login eseguito controllando che ci sia COSA VUOI FARE?
        assert "COSA VUOI FARE?" in driver.find_element_by_css_selector('#selenium-highlight > div > h1').text
        print("LOGIN OK")
    finally:
        time.sleep(3)

    print("Clicco su 'Contenuti Publici'")
    driver.find_element_by_css_selector(
        '#selenium-highlight > div > div.tabella_cosa_vuoi_fare > table > tbody > tr:nth-child(1) > td:nth-child(2)').click()
    time.sleep(3)

    driver.find_element_by_xpath('//*[@id="index-pagination"]/a').click()
    print("ho cliccato su i risultati in fondo alla pagina")
    time.sleep(3)
    try:
        element = WebDriverWait(driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="open-menu-button"]/button'))
        )
        driver.find_element_by_xpath('//*[@id="open-menu-button"]/button').click()
        print("ho cliccato su menu")
    finally:
        time.sleep(3)
        print("sto per cliccare su 'Il Progetto'")
    #
    # driver.find_element_by_css_selector("a").click()
    #
    try:
        time.sleep(5)
        element = WebDriverWait(driver,50).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, 'a'))
        )
        driver.find_element_by_css_selector('a').click()
        print("ho cliccato su 'Il Progetto'")
    finally:
        time.sleep(5)
        print("sto per cliccare su menu")

    try:
        element = WebDriverWait(driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="open-menu-button"]/button'))
        )
        driver.find_element_by_xpath('//*[@id="open-menu-button"]/button').click()
        print("ho cliccato su menu")
    finally:
        time.sleep(3)
        print("sto per cliccare su 'I Risultati'")

    try:
        time.sleep(5)
        element = WebDriverWait(driver,50).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, '#menu-item-154 > a'))
        )
        driver.find_element_by_css_selector('#menu-item-154 > a').click()
        print("ho cliccato su 'I Risultati'")
    finally:
        time.sleep(5)
        print("sto per cliccare su menu")

    try:
        element = WebDriverWait(driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="open-menu-button"]/button'))
        )
        driver.find_element_by_xpath('//*[@id="open-menu-button"]/button').click()
        print("ho cliccato su menu")
    finally:
        time.sleep(3)
        print("sto per cliccare su 'Contatti'")

    try:
        time.sleep(5)
        element = WebDriverWait(driver,50).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, '#menu-item-61 > a'))
        )
        driver.find_element_by_css_selector('#menu-item-61 > a').click()
        print("ho cliccato su 'Contatti'")
    finally:
        time.sleep(5)

    time.sleep(5)
    driver.find_element_by_name('your-name').click()
    driver.find_element_by_name('your-name').send_keys("Pasquale")
    time.sleep(3)

    driver.find_element_by_name('your-email').click()
    driver.find_element_by_name('your-email').send_keys("dall@mailinator.com")
    time.sleep(3)

    driver.find_element_by_name('your-message').click()
    driver.find_element_by_name('your-message').send_keys("Test prova")
    time.sleep(3)

    driver.find_element_by_xpath('//*[@id="wpcf7-f58-o1"]/form/table[1]/tbody/tr[2]/td[2]/label/input').click()

    driver.execute_script("window.scrollTo(document.body.scrollHeight, 0);")
    time.sleep(3)

    try:
        element = WebDriverWait(driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="open-menu-button"]/button'))
        )
        driver.find_element_by_xpath('//*[@id="open-menu-button"]/button').click()
        print("ho cliccato su menu")
    finally:
        time.sleep(3)
        print("sto per cliccare su 'Join'")

    driver.find_element_by_css_selector("#menu-item-38 > a").click()
    time.sleep(3)
    driver.find_element_by_css_selector("button.button-page-join > a").click()
    print("ho cliccato su contattaci")

    try:
        time.sleep(3)
        element = WebDriverWait(driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="open-menu-button"]/button'))
        )
        driver.find_element_by_xpath('//*[@id="open-menu-button"]/button').click()
        print("ho cliccato su menu")
    finally:
        time.sleep(3)

    print("clicco su 'BIGDATA Platform'")
    driver.find_element_by_css_selector("#menu-item-162 > a").click()

    try:
        time.sleep(3)
        element = WebDriverWait(driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="open-menu-button"]/button'))
        )
        driver.find_element_by_xpath('//*[@id="open-menu-button"]/button').click()
        print("ho cliccato su menu")
    finally:
        time.sleep(3)

    print("clicco su 'AI Platform'")
    driver.find_element_by_css_selector("#menu-item-161 > a").click()
    time.sleep(5)

    try:
        time.sleep(3)
        element = WebDriverWait(driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="open-menu-button"]/button'))
        )
        driver.find_element_by_xpath('//*[@id="open-menu-button"]/button').click()
        print("ho cliccato su menu")
    finally:
        time.sleep(3)

    print("clicco su 'IOT Platform'")
    driver.find_element_by_css_selector("#menu-item-287 > a").click()
    time.sleep(5)
    print("clicco sul prototipo")
    driver.find_element_by_css_selector("span.mb-text").click()
    time.sleep(5)

    print("torno indietro")
    driver.back()

    try:
        time.sleep(3)
        element = WebDriverWait(driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="open-menu-button"]/button'))
        )
        driver.find_element_by_xpath('//*[@id="open-menu-button"]/button').click()
        print("ho cliccato su menu")
    finally:
        time.sleep(3)

    print("clicco su 'SECURITY&PRIVACY Platform'")
    driver.find_element_by_css_selector("#menu-item-159 > a").click()

    try:
        time.sleep(3)
        element = WebDriverWait(driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="open-menu-button"]/button'))
        )
        driver.find_element_by_xpath('//*[@id="open-menu-button"]/button').click()
        print("ho cliccato su menu")
    finally:
        time.sleep(3)

    print("clicco su 'APP Platform'")
    driver.find_element_by_css_selector("#menu-item-158 > a").click()
    time.sleep(5)

    try:
        time.sleep(3)
        element = WebDriverWait(driver, 50).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="open-menu-button"]/button'))
        )
        driver.find_element_by_xpath('//*[@id="open-menu-button"]/button').click()
        print("ho cliccato su menu")
    finally:
        time.sleep(3)

    print("sto per uscire dal sito")
    driver.find_element_by_css_selector("#menu-item-83 > a").click()
    time.sleep(3)

    try:
        # assert di logout eseguito controllando che ritorno alla pagina di login e che ci sia BENVENUTO
        assert "BENVENUTO" in driver.find_element_by_css_selector(
            '#page-main-article-content > div.rettangolo_accedi > h2').text
        print("LOGOUT OK")
    finally:
        time.sleep(3)


test_navigate_site_editore()

driver.quit()
print("END " + nomeTest)

# invio mail di report
from ReportToEmail import ReportToEmail

report = ReportToEmail()

data = time.strftime("%d/%m/%Y - ore %H:%M:%S")
oggetto = nomeTest + ' - OK - eseguito il ' + data
messaggio = 'END ' + nomeTest
report.sendemail(subject=oggetto, message=messaggio)

unittest.main()